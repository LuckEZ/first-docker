FROM liliancal/formation-devops:1.1
ADD src /var/www/
EXPOSE 89
CMD ["/usr/sbin/apache2ctl" "-D" "FOREGROUND"]
